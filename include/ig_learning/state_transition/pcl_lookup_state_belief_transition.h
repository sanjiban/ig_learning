/* Copyright 2015 Sanjiban Choudhury
 * pcl_lookup_state_belief_transition.h
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_BELIEF_TRANSITION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_BELIEF_TRANSITION_H_


#include "ig_learning/state_transition.h"
#include "ig_active_reconstruction_octomap/octomap_std_pcl_input_point_xyz.hpp"
#include "ig_active_reconstruction_octomap/octomap_ig_tree_world_representation.hpp"
#include "ig_active_reconstruction_octomap/octomap_ray_occlusion_calculator.hpp"

namespace ig_learning {

class PclLookupStateBeliefTransition : public StateTransition {
public:
  typedef ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation WorldRepresentation;
  typedef WorldRepresentation::TreeType TreeType;
  typedef ig_active_reconstruction::world_representation::octomap::StdPclInputPointXYZ<TreeType>::PclType PclType;

  PclLookupStateBeliefTransition()
  : input_config_(),
    occlusion_config_(0.3) {}

  virtual ~PclLookupStateBeliefTransition() {}

  virtual bool UpdateState(const State &input, const Action &action, const WorldMap &world_map, State &output) const;

  bool Initialize(ros::NodeHandle &nh);

private:
  ig_active_reconstruction::world_representation::octomap::StdPclInputPointXYZ<TreeType>::Type::Config input_config_;
  ig_active_reconstruction::world_representation::octomap::RayOcclusionCalculator<TreeType,PclType>::Options occlusion_config_;
};

typedef boost::shared_ptr<PclLookupStateBeliefTransition> PclLookupStateBeliefTransitionPtr;


}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_BELIEF_TRANSITION_H_ */
