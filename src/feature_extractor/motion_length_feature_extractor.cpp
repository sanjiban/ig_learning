/* Copyright 2015 Sanjiban Choudhury
 * motion_length_feature_extractor.cpp
 *
 *  Created on: Jul 21, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/feature_extractor/motion_length_feature_extractor.h"

namespace ig_learning {

void MotionLengthFeatureExtractor::GetFeature(const State &state, const Action &action, std::vector<double> &feature) const {
  feature.clear();
  double distance = 0;
  std::vector<Action> action_set = state.action_set;
  action_set.push_back(action);
  for (unsigned int i = 0; i < action_set.size() - 1; i++) {
    distance += (action_set[i+1].view.pose().position -
        action_set[i].view.pose().position).norm();
  }
  feature.push_back(distance);
}

}


