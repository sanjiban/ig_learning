/* Copyright 2015 Sanjiban Choudhury
 * forward_training.cpp
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/imitation_learning/forward_training.h"
#include "ig_learning/state_utils.h"

namespace ig_learning {

void ForwardTraining::Train(const Input &input, Output &output) const {
  std::vector< std::pair<State, WorldMap> > train_dataset = input.train_dataset;

  output.model_set.clear();
  for (unsigned int time_step = 0; time_step < input.total_timesteps; time_step++) {
    std::vector <double> tmp;
    params_.feature_fn->GetFeature(train_dataset.front().first,
                                   *train_dataset.front().second.action_set.begin(),
                                    tmp);
    unsigned int fdim = tmp.size();
    std::vector<Eigen::MatrixXd> feature_table;
    std::vector<Eigen::MatrixXd> feature_table_all_actions;
    std::vector<Eigen::VectorXd> qval_table;

    unsigned int datapoint = 0;
    for (auto &it : train_dataset) {
      ROS_ERROR_STREAM("Time step: "<<time_step<<" Datapoint: "<<datapoint);

      std::set<Action> actions_selected_all = it.second.action_set; // TODO
      for (auto a : it.first.action_set)
        actions_selected_all.erase(a);

      std::set<Action> actions_selected = params_.action_selection.SelectActions(actions_selected_all);
      Eigen::MatrixXd feature_point(actions_selected.size(), fdim);
      Eigen::MatrixXd feature_point_all_actions(actions_selected_all.size(), fdim);
      Eigen::VectorXd qval_point(actions_selected.size());
      unsigned int id = 0;
      unsigned int id_all = 0;

      for (auto a : actions_selected_all) {
        std::vector<double> feature_vec;
        params_.feature_fn->GetFeature(it.first, a, feature_vec);
        for (unsigned int fidx = 0; fidx < feature_vec.size(); fidx++)
          feature_point_all_actions(id_all, fidx) = feature_vec[fidx];
        id_all++;
        if (actions_selected.count(a) > 0) {
          for (unsigned int fidx = 0; fidx < feature_vec.size(); fidx++)
            feature_point(id, fidx) = feature_vec[fidx];

          double qval = params_.oracle_fn->Value(it.first, a, it.second, input.total_timesteps - (time_step+1));
          qval_point[id] = qval;

          id++;
        }
      }

      feature_table_all_actions.push_back(feature_point_all_actions);
      feature_table.push_back(feature_point);
      qval_table.push_back(qval_point);

      datapoint++;
    }

    CSClassificationPtr model(params_.cs_class_fn->Clone());
    model->Train(feature_table, qval_table);
    output.model_set.push_back(model);

    //Make predictions
    unsigned int id = 0;
    for (auto &it: train_dataset) {
      std::set<Action> actions_selected_all = it.second.action_set; // TODO
      for (auto a : it.first.action_set)
        actions_selected_all.erase(a);
      Eigen::MatrixXd feature_all_actions = feature_table_all_actions[id];

      unsigned int selected_index = model->Predict(feature_all_actions);
      Action selected_action = *std::next(actions_selected_all.begin(), selected_index);
      State new_state;
      params_.state_belief_transition->UpdateState(it.first, selected_action, it.second, new_state);

      if (params_.cost_fn->Cost(new_state, it.second) <= params_.cost_budget) {
        it.first = new_state;
      }

      id++;
    }
  }
}

std::set<Action> ForwardTraining::ActionSelection::SelectActions(const std::set<Action> &action_selection) const{
  switch (mode_) {
    case ActionSelection::ALL: {
      return action_selection;
      break;
    }
    case ActionSelection::SUBSET: {
      std::vector<Action> rearrange_actions;
      std::copy(action_selection.begin(), action_selection.end(), std::back_inserter(rearrange_actions));
      std::random_shuffle(rearrange_actions.begin(), rearrange_actions.end());
      std::set<Action> actions_to_send;
      unsigned int id = 0;
      if (num_selection_ == 0)
        return std::set<Action>();
      for (auto it : rearrange_actions) {
        actions_to_send.insert(it);
        id++;
        if (id >= num_selection_ || id >= rearrange_actions.size())
          break;
      }
      return actions_to_send;
      break;
    }
    default:
      return std::set<Action>();
  }
}


}

