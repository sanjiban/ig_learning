/* Copyright 2015 Sanjiban Choudhury
 * offline_solver_clairvoyant_oracle.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/clairvoyant_oracles/offline_solver_clairvoyant_oracle.h"

namespace ig_learning {

double OfflineSolverClairvoyantOracle::Value(const State &state, const Action &action, const WorldMap &world_map, unsigned int time_steps) const {
  State new_state;
  input_.state_transition->UpdateState(state, action, world_map, new_state);

  OfflineSolverParams input(input_);
  input.state0 = State(new_state);
  input.world_map = world_map;
  input.budget = time_steps;

  OfflineSolverOutput output;
  solver_->Solve(input, output);

  return output.objective_trajectory.back() - input.obj_fn->Value(state, world_map);
}


}




