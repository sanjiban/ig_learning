/* Copyright 2015 Sanjiban Choudhury
 * occupied_cells_objective_function.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/objective_functions/occupied_cells_objective_function.h"

namespace ig_learning {

OccupiedCellsObjectiveFunction::OccupiedCellsObjectiveFunction()
: bbox_lb_(-std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity()),
  bbox_ub_(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity()) {
}

OccupiedCellsObjectiveFunction::OccupiedCellsObjectiveFunction(Eigen::Vector3d bbox_lb, Eigen::Vector3d bbox_ub)
: bbox_lb_(bbox_lb),
  bbox_ub_(bbox_ub) {}

double OccupiedCellsObjectiveFunction::Value(const State &input, const WorldMap &world_map) const{
  typedef Belief::TreeType TreeType;
  BeliefInterface<TreeType>::Config config;
  config.world_frame_name = "world";
  State state(input);
  BeliefInterface<TreeType>::Ptr obj = state.belief.getLinkedObj<BeliefInterface>(config);
  return obj->GetCellCountInBBox(bbox_lb_, bbox_ub_);
}

}


