/* Copyright 2015 Sanjiban Choudhury
 * lazy_greedy_nemhauser.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/offline_solvers/lazy_greedy_nemhauser.h"

namespace ig_learning {

void LazyGreedyNemhauser::Solve(const OfflineSolverParams &input, OfflineSolverOutput &output) const {
  State state(input.state0);
  double val = input.obj_fn->Value(state, input.world_map);

  std::vector< std::pair<Action, double> > marginal_gain_set;
  auto cmp = [](std::pair<Action, double> const & a, std::pair<Action, double> const & b) {
       return a.second > b.second;
  };
  for (auto it : input.world_map.action_set)
    marginal_gain_set.emplace_back(it, std::numeric_limits<double>::infinity());

  double best_val = -1;
  std::vector< std::pair<Action, double> >::iterator best_action;
  for (int k = 0; k < input.budget; k++) {
    for (auto it = marginal_gain_set.begin(); it != marginal_gain_set.end(); ++it) {
      if (best_val < it->second) {
        State state_new;
        input.state_transition->UpdateState(state, it->first, input.world_map, state_new);
        double marginal_gain = input.obj_fn->Value(state_new, input.world_map) - val;
        //ROS_ERROR_STREAM("Evaluated : "<<it->first.id<<" old gain "<<it->second << " new gain "<<marginal_gain <<" best val"<<best_val);
        it->second = marginal_gain;
        if (marginal_gain > best_val) {
          best_val = marginal_gain;
          best_action = it;
        }
      } else {
        break;
      }
    }

    //State state_new;
    input.state_transition->UpdateState(state, best_action->first, input.world_map, state);
    //state = state_new;

    val = input.obj_fn->Value(state, input.world_map);

    output.objective_trajectory.push_back(val);
    output.cost_trajectory.push_back(input.cost_fn->Cost(state, input.world_map));

    //ROS_ERROR_STREAM("k: "<<k<<" action: "<<best_action->first.id<<" val: "<<val);

    marginal_gain_set.erase(best_action);
    std::sort(marginal_gain_set.begin(), marginal_gain_set.end(), cmp);
    best_val = -1;
  }

  output.action_sequence = state.action_set;
  output.state_final = state;
}


}


