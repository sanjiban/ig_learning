/* Copyright 2015 Sanjiban Choudhury
 * generalized_cost_benefit.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/offline_solvers/generalized_cost_benefit.h"
#include "ig_learning/offline_solvers/tsp_solver.h"

namespace ig_learning {

void GeneralizedCostBenefit::Solve(const OfflineSolverParams &input, OfflineSolverOutput &output) const {
  State state(input.state0);
  State start_state(state);
  double val = input.obj_fn->Value(state, input.world_map);
  double curr_cost = input.cost_fn->Cost(state, input.world_map);

  std::map< Action, double > marginal_gain_set, cost_gain_set, ratio_set;
  auto cmp_inc = [](std::pair<Action, double> const & a, std::pair<Action, double> const & b) {
    return a.second < b.second;
  };

  bool recompute = true;
  std::set<Action> action_set = input.world_map.action_set;
  unsigned counter = 0;
  while (true) {
    if (recompute) {
      recompute = false;
      marginal_gain_set.clear();
      cost_gain_set.clear();
      ratio_set.clear();

      for (auto it : action_set) {
        State state_new;
        input.state_transition->UpdateState(state, it, input.world_map, state_new);
        double marginal_gain = input.obj_fn->Value(state_new, input.world_map) - val;
        double cost_gain = MotionAndTourCost(state_new, input) - curr_cost;
        double ratio = marginal_gain / std::max(std::numeric_limits<double>::epsilon(), cost_gain);

        marginal_gain_set[it] = marginal_gain;
        cost_gain_set[it] = cost_gain;
        ratio_set[it] = ratio;
      }
    }

    auto ratio_max = std::max_element(ratio_set.begin(), ratio_set.end(), cmp_inc);
    Action best_action = ratio_max->first;

    if (counter < input.budget &&
        curr_cost + cost_gain_set[best_action] < input.cost_budget &&
        marginal_gain_set[best_action] > 0) {
      input.state_transition->UpdateState(state, best_action, input.world_map, state);
      val += marginal_gain_set[best_action];
      curr_cost += cost_gain_set[best_action];
      recompute = true;
      counter++;
     // ROS_ERROR_STREAM("val "<<val <<" curr "<<curr_cost);
    }

    action_set.erase(best_action);
    marginal_gain_set.erase(best_action);
    cost_gain_set.erase(best_action);
    ratio_set.erase(best_action);
    if (action_set.size() == 0 || counter >= input.budget)
      break;
  }
  RearrangeActionsOptimalTour(state, input, output.state_final);
  RecomputeObjectiveCost(output.state_final, input, output);
  output.action_sequence = output.state_final.action_set;
}


double GeneralizedCostBenefit::MotionAndTourCost(const State &state,
                                                 const OfflineSolverParams &input) const{
  double old_cost = input.cost_fn->Cost(input.state0, input.world_map);
  State inc;
  inc.action_set = std::vector<Action>(state.action_set.begin() +
                                       (input.state0.action_set.size() - 1),
                                       state.action_set.end());
  return old_cost + tsp_solver::GetTourCost(inc);
}

void GeneralizedCostBenefit::RearrangeActionsOptimalTour(const State &state,
                                                         const OfflineSolverParams &input,
                                                         State &output) const{
  State inc, out_inc;
  inc.action_set = std::vector<Action>(state.action_set.begin() +
                                       (input.state0.action_set.size() - 1),
                                       state.action_set.end());
  inc.observation_set = std::vector<Observation>(state.observation_set.begin() +
                                                 (input.state0.observation_set.size() - 1),
                                                 state.observation_set.end());

  tsp_solver::GetOptimizedTour(inc, out_inc);
  output = State(input.state0);
  for (unsigned int i = 1; i < out_inc.action_set.size(); i++) {
    output.action_set.push_back(out_inc.action_set[i]);
    output.observation_set.push_back(out_inc.observation_set[i]);
  }
}

void GeneralizedCostBenefit::RecomputeObjectiveCost(const State &state,
                                                    const OfflineSolverParams &input,
                                                    OfflineSolverOutput &output) const {
  for (unsigned int i = input.state0.action_set.size(); i <= state.action_set.size(); i++) {
    State inc;
    inc.action_set = std::vector<Action>(state.action_set.begin(),
                                         state.action_set.begin() + i);
    inc.observation_set = std::vector<Observation>(state.observation_set.begin(),
                                                   state.observation_set.begin() + i);
    output.objective_trajectory.push_back(input.obj_fn->Value(inc, input.world_map));
    output.cost_trajectory.push_back(input.cost_fn->Cost(inc, input.world_map));
  }
}


}

