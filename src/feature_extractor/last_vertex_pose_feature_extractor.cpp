/* Copyright 2015 Sanjiban Choudhury
 * last_vertex_pose_feature_extractor.cpp
 *
 *  Created on: Jul 7, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/feature_extractor/last_vertex_pose_feature_extractor.h"
#include "tf_conversions/tf_eigen.h"

namespace ig_learning {

void LastVertexPoseFeatureExtractor::GetFeature(const State &state, const Action &action, std::vector<double> &feature) const {
  feature.clear();
  movements::Pose last_vertex = state.action_set.back().view.pose();
  movements::Pose new_vertex = action.view.pose();

  switch (mode_) {
    case ABS_POS_ONLY: {
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(last_vertex.position[i]);
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(new_vertex.position[i]);
      break;
    }
    case REL_POS_ONLY: {
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(last_vertex.position[i] - new_vertex.position[i]);
      break;
    }
    case ABS_POS_ORIENT: {
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(last_vertex.position[i]);
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(new_vertex.position[i]);
      Eigen::Vector3d last_euler = RPYFromQuat(last_vertex.orientation);
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(last_euler[i]);
      Eigen::Vector3d new_euler = RPYFromQuat(new_vertex.orientation);
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(new_euler[i]);
      break;
    }
    case REL_POS_ORIENT: {
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(last_vertex.position[i] - new_vertex.position[i]);
      Eigen::Vector3d last_euler = RPYFromQuat(last_vertex.orientation);
      Eigen::Vector3d new_euler = RPYFromQuat(new_vertex.orientation);
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(last_euler[i] - new_euler[i]);
      break;
    }
    case REL_POS_ORIENT_MAG: {
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(last_vertex.position[i] - new_vertex.position[i]);
      Eigen::Vector3d last_euler = RPYFromQuat(last_vertex.orientation);
      Eigen::Vector3d new_euler = RPYFromQuat(new_vertex.orientation);
      for (std::size_t i = 0; i < 3; i++)
        feature.push_back(last_euler[i] - new_euler[i]);
      feature.push_back((last_vertex.position - new_vertex.position).norm());
      break;
    }
  }
}

Eigen::Vector3d LastVertexPoseFeatureExtractor::RPYFromQuat(const Eigen::Quaterniond &quat) const {
  // SHAMELESS !!!
  tf::Quaternion tf_quat;
  tf::quaternionEigenToTF(quat, tf_quat);
  tf::Matrix3x3 m(tf_quat);
  Eigen::Vector3d euler;
  m.getRPY(euler.x(), euler.y(), euler.z());
  return euler;
}


}



