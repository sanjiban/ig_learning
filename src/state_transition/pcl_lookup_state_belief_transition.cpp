/* Copyright 2015 Sanjiban Choudhury
 * pcl_lookup_state_transition.cpp
 *
 *  Created on: Jun 28, 2016
 *      Author: Sanjiban Choudhury
 */


#include "ig_learning/state_transition/pcl_lookup_state_belief_transition.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_active_reconstruction/view_space.hpp"

namespace iar = ig_active_reconstruction;
using namespace iar::world_representation::octomap;

namespace ig_learning {

bool PclLookupStateBeliefTransition::UpdateState(const State &input, const Action &action, const WorldMap &world_map, State &output) const{
  output = input;
  pcl::PointCloud<pcl::PointXYZ> cloud, cloud_tr;
  try {
    cloud = world_map.pcl_lookup.at(action.id); // Should exist by construction
  } catch (const std::out_of_range& e) {
    ROS_ERROR_STREAM("No pcl in lookup");
    return false;
  }

  Eigen::Transform<double,3,Eigen::Affine> sensor_to_world_transform;
  sensor_to_world_transform.fromPositionOrientationScale(action.view.pose().position, action.view.pose().orientation, Eigen::Vector3d::Ones());
  pcl::transformPointCloud(cloud, cloud_tr, sensor_to_world_transform);
  output.action_set.push_back(action);
  output.observation_set.push_back(cloud_tr);

  typename StdPclInputPointXYZ<TreeType>::Ptr std_input = output.belief.getLinkedObj<StdPclInputPointXYZ>(input_config_);
  std_input->setOcclusionCalculator<ig_active_reconstruction::world_representation::octomap::RayOcclusionCalculator>(occlusion_config_);
  std_input->push(sensor_to_world_transform, cloud);

  return true;
}

bool PclLookupStateBeliefTransition::Initialize(ros::NodeHandle &nh) {
  // Load input config
  ros_tools::getParamIfAvailable(input_config_.use_bounding_box,"use_bounding_box", nh);
  ros_tools::getParamIfAvailable<float,double>(input_config_.bounding_box_min_point_m.x(),"bounding_box_min_point_m/x", nh);
  ros_tools::getParamIfAvailable<float,double>(input_config_.bounding_box_min_point_m.y(),"bounding_box_min_point_m/y", nh);
  ros_tools::getParamIfAvailable<float,double>(input_config_.bounding_box_min_point_m.z(),"bounding_box_min_point_m/z", nh);
  ros_tools::getParamIfAvailable<float,double>(input_config_.bounding_box_max_point_m.x(),"bounding_box_max_point_m/x", nh);
  ros_tools::getParamIfAvailable<float,double>(input_config_.bounding_box_max_point_m.y(),"bounding_box_max_point_m/y", nh);
  ros_tools::getParamIfAvailable<float,double>(input_config_.bounding_box_max_point_m.z(),"bounding_box_max_point_m/z", nh);
  ros_tools::getParamIfAvailable(input_config_.max_sensor_range_m,"max_sensor_range_m", nh);

  //Load occlusion
  ros_tools::getParamIfAvailable(occlusion_config_.occlusion_update_dist_m,"occlusion_update_dist_m", nh);

  return true;
}

}

