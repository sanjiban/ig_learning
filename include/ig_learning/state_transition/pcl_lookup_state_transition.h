/* Copyright 2015 Sanjiban Choudhury
 * pcl_lookup_state_transition.h
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_TRANSITION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_TRANSITION_H_

#include "ig_learning/state_transition.h"

namespace ig_learning {

class PclLookupStateTransition : public StateTransition {
public:
  PclLookupStateTransition() {}

  virtual ~PclLookupStateTransition() {}

  virtual bool UpdateState(const State &input, const Action &action, const WorldMap &world_map, State &output) const;

};

typedef boost::shared_ptr<PclLookupStateTransition> PclLookupStateTransitionPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_TRANSITION_H_ */
