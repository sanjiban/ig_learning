/* Copyright 2015 Sanjiban Choudhury
 * pcl_lookup_state_transition.cpp
 *
 *  Created on: Jul 5, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/state_transition/pcl_lookup_state_transition.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_active_reconstruction/view_space.hpp"
#include <pcl/common/transforms.h>

namespace iar = ig_active_reconstruction;
using namespace iar::world_representation::octomap;

namespace ig_learning {

bool PclLookupStateTransition::UpdateState(const State &input, const Action &action, const WorldMap &world_map, State &output) const{
  output = input;
  pcl::PointCloud<pcl::PointXYZ> cloud, cloud_tr;
  try {
    cloud = world_map.pcl_lookup.at(action.id); // Should exist by construction
  } catch (const std::out_of_range& e) {
    ROS_ERROR_STREAM("No pcl in lookup");
    return false;
  }

  Eigen::Transform<double,3,Eigen::Affine> sensor_to_world_transform;
  sensor_to_world_transform.fromPositionOrientationScale(action.view.pose().position, action.view.pose().orientation, Eigen::Vector3d::Ones());
  pcl::transformPointCloud(cloud, cloud_tr, sensor_to_world_transform);

  output.action_set.push_back(action);
  output.observation_set.push_back(cloud_tr);

  return true;
}

}



