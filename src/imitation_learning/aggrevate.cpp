/* Copyright 2015 Sanjiban Choudhury
 * aggrevate.cpp
 *
 *  Created on: Jul 30, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/imitation_learning/aggrevate.h"
#include "ig_learning/state_utils.h"

namespace ig_learning {

void Aggrevate::Train(const Input &input, Output &output) const {
  std::vector< Input::DataPoint > train_dataset = input.train_dataset;

  output.model_set.clear();
  std::vector<Eigen::MatrixXd> feature_table;
  std::vector<Eigen::VectorXd> qval_table;
  CSClassificationPtr current_model(params_.cs_class_fn->Clone());

  for (unsigned int iters_aggrevate = 0; iters_aggrevate < params_.max_iters_aggrevate; iters_aggrevate++) {
    double beta = params_.beta_fn(iters_aggrevate);

    unsigned int datapoint = 0;
    for (auto &it : train_dataset) {
      ROS_ERROR_STREAM("Aggrev: "<<iters_aggrevate<<" Datapoint: "<<datapoint);

      State state0 = it.state0;
      WorldMap world_map = it.map;
      double t = std::rand() % (input.total_timesteps - 1);

      State state;
      if ( ((double) std::rand() / (RAND_MAX))  <= beta ) {
        //ROS_ERROR_STREAM("precompute expert");
        RollOutPrecomputedOracle(state0, world_map, t, it.precomputed_expert, state);

        /*
        for (auto it : state.action_set)
          std::cerr << it.id <<" ";
        std::cerr <<"\n";
        ROS_ERROR_STREAM("Val: "<<params_.objective_fn->Value(state, world_map) <<" Cost: "<<params_.cost_fn->Cost(state, world_map));
        */
      } else {
        //ROS_ERROR_STREAM("learner");

        RollOutLearner(state0, world_map, t, current_model, state);

        /*
        for (auto it : state.action_set)
          std::cerr << it.id <<" ";
        std::cerr <<"\n";
        ROS_ERROR_STREAM("Val: "<<params_.objective_fn->Value(state, world_map) <<" Cost: "<<params_.cost_fn->Cost(state, world_map));
        */
      }

      std::set<Action> actions_selected_all = world_map.action_set;
      for (auto a : state0.action_set)
        actions_selected_all.erase(a);

      std::set<Action> actions_selected = params_.action_selection.SelectActions(actions_selected_all);

      std::vector< std::pair<std::vector<double>, double> > data_point;
      for (auto a : actions_selected) {
        std::vector<double> feature_vec;
        params_.feature_fn->GetFeature(state, a, feature_vec);
        double qval = params_.oracle_fn->Value(state, a, world_map, input.total_timesteps - (t+1));
        data_point.emplace_back(feature_vec, qval);
      }
      Eigen::MatrixXd feature_point(data_point.size(), data_point.front().first.size());
      Eigen::VectorXd qval_point(data_point.size());
      for (unsigned int i = 0; i < data_point.size(); i++) {
        for (unsigned int j = 0; j <  data_point.front().first.size(); j++)
          feature_point(i, j) = data_point[i].first[j];
        qval_point(i) = data_point[i].second;
      }
      feature_table.push_back(feature_point);
      qval_table.push_back(qval_point);
      datapoint++;
    }

    // update model
    CSClassificationPtr model(params_.cs_class_fn->Clone());
    model->Train(feature_table, qval_table);
    output.model_set.push_back(model);
    current_model = model;

    // save model
    std::string model_filename = params_.model_filename_prefix + std::to_string(iters_aggrevate)+".pred";;
    model->Save(model_filename);
    output.model_set.push_back(model);

    //save data

    // validate!
    double mean_obj = 0;
    for (unsigned int i = 0; i < 3; i++) {
      ROS_ERROR_STREAM("Evaluate: "<<i);
      State state0 = train_dataset[i].state0;
      WorldMap world_map = train_dataset[i].map;
      State state;
      RollOutLearner(state0, world_map, input.total_timesteps, current_model, state);
      mean_obj += params_.objective_fn->Value(state, world_map);
    }
    mean_obj /= 3.0;
    ROS_ERROR_STREAM("Eval mean: "<<mean_obj);
  }
}

void Aggrevate::RollOutLearner(const State &state0, const WorldMap &world_map, double tau, const CSClassificationPtr &model, State &state) const {
  state = state0;
  for (unsigned int t = 0; t < tau; t++) {
    std::set<Action> action_set = world_map.action_set;
    for (auto a : state.action_set)
      action_set.erase(a);


    std::vector <double> tmp;
    params_.feature_fn->GetFeature(state, *action_set.begin(), tmp);
    unsigned int fdim = tmp.size();

    Eigen::MatrixXd feature_point(action_set.size(), fdim);
    unsigned int id = 0;
    for (auto a : action_set) {
      std::vector<double> feature_vec;
      params_.feature_fn->GetFeature(state, a, feature_vec);
      for (unsigned int fidx = 0; fidx < feature_vec.size(); fidx++)
        feature_point(id, fidx) = feature_vec[fidx];
      id++;
    }

    unsigned int selected_index = model->Predict(feature_point);
    Action selected_action = *std::next(action_set.begin(), selected_index);

    State new_state;
    params_.state_belief_transition->UpdateState(state, selected_action, world_map, new_state);
    if (params_.cost_fn->Cost(new_state, world_map) <= params_.cost_budget) {
      state = new_state;
    } else {
      break;
    }
  }
}

void Aggrevate::RollOutPrecomputedOracle(const State &state0, const WorldMap &world_map, double tau, const std::vector<Action> &action_sequence, State &state) const {
  state = state0;
  for (unsigned int t = 0; t < tau; t++) {
    if (t >= action_sequence.size())
      break;
    Action selected_action = action_sequence[t];
    State new_state;
    params_.state_belief_transition->UpdateState(state, selected_action, world_map, new_state);
    if (params_.cost_fn->Cost(new_state, world_map) <= params_.cost_budget) {
      state = new_state;
    } else {
      break;
    }
  }
}


std::set<Action> Aggrevate::ActionSelection::SelectActions(const std::set<Action> &action_selection) const{
  switch (mode_) {
    case ActionSelection::ALL: {
      return action_selection;
      break;
    }
    case ActionSelection::SUBSET: {
      std::vector<Action> rearrange_actions;
      std::copy(action_selection.begin(), action_selection.end(), std::back_inserter(rearrange_actions));
      std::random_shuffle(rearrange_actions.begin(), rearrange_actions.end());
      std::set<Action> actions_to_send;
      unsigned int id = 0;
      if (num_selection_ == 0)
        return std::set<Action>();
      for (auto it : rearrange_actions) {
        actions_to_send.insert(it);
        id++;
        if (id >= num_selection_ || id >= rearrange_actions.size())
          break;
      }
      return actions_to_send;
      break;
    }
    default:
      return std::set<Action>();
  }
}


}



