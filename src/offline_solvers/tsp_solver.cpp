/* Copyright 2015 Sanjiban Choudhury
 * tsp_solver.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/offline_solvers/tsp_solver.h"
#include "ig_learning/offline_solvers/LKMatrix.h"

namespace ig_learning {
namespace tsp_solver {

double GetTourCost (const State &input_state) {
  std::vector < std::vector <double> > edge_distance = GetEdgeDistance(input_state);
  LKMatrix solver(edge_distance);
  solver.optimizeTour();
  std::vector<int> tour = solver.getCurrentTour();
  double dist = 0;
  for (unsigned int i = 0; i < tour.size()-1; i++)
    dist += edge_distance[tour[i]][tour[i+1]];
  return dist;
}

void GetOptimizedTour (const State &input_state, State &output_state) {
  std::vector < std::vector <double> > edge_distance = GetEdgeDistance(input_state);
  LKMatrix solver(edge_distance);
  solver.optimizeTour();
  std::vector<int> tour = solver.getCurrentTour();

  output_state = State(input_state);
  for (unsigned int i = 0; i < tour.size(); i++) {
    output_state.action_set[i] = input_state.action_set[tour[i]];
    output_state.observation_set[i] = input_state.observation_set[tour[i]];
  }
}

std::vector < std::vector <double> > GetEdgeDistance(const State &state) {
  std::vector < std::vector <double> >  edge_distance(state.action_set.size(),
                                                      std::vector <double>(state.action_set.size(), 0.0));

  for (unsigned int i = 0; i < state.action_set.size(); i++) {
    for (unsigned int j = 0; j < state.action_set.size(); j++) {
      edge_distance[i][j] = (state.action_set[i].view.pose().position -
                             state.action_set[j].view.pose().position).norm();
      edge_distance[j][i] = edge_distance[i][j];
    }
  }

  return edge_distance;
}

}
}


