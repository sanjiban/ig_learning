/* Copyright 2015 Sanjiban Choudhury
 * state.h
 *
 *  Created on: Jun 28, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_STATE_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_STATE_H_

#include <stdint.h>
#include <ros/ros.h>
#include "ig_active_reconstruction/view.hpp"
#include "ig_active_reconstruction/view_space.hpp"
#include <pcl_ros/point_cloud.h>
#include "ig_active_reconstruction_octomap/octomap_ig_tree_world_representation.hpp"
#include "visualization_msgs/MarkerArray.h"

namespace ig_learning {

/**
 * \brief Action class
 * almost a POD except providing id
 */
struct Action {
  ig_active_reconstruction::views::View view;
  unsigned int id;
  bool operator < (const Action &other) const {return id < other.id;}
};

typedef pcl::PointCloud<pcl::PointXYZ> Observation;
typedef ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation Belief;

/**
 * \brief POD representing state - actions taken so far, observations received so far and belief
 * It is overparameterized to aid in calculation of different functions
 */
struct State {
  std::vector <Action> action_set;
  std::vector <Observation> observation_set;
  Belief belief;

  State()
  : action_set(),
    observation_set(),
    belief() {}

  State(const State &other)
  : action_set(other.action_set),
    observation_set(other.observation_set),
    belief(other.belief) {}

  State& operator=( const State& other ) {
    action_set = other.action_set;
    observation_set = other.observation_set;
    belief = other.belief;
    return *this;
  }

};

struct WorldMap {
  pcl::PointCloud<pcl::PointXYZ> model_pcl;
  double res;
  std::map <unsigned int, pcl::PointCloud<pcl::PointXYZ> > pcl_lookup;
  std::set<Action> action_set;
};

visualization_msgs::MarkerArray VisualizeBelief(const State &state);

pcl::PointCloud<pcl::PointXYZ> VisualizeObservation(const State &state);


bool InitializeState(ros::NodeHandle &n, State &state);

template<class TREE_TYPE>
class BeliefInterface : public ig_active_reconstruction::world_representation::octomap::WorldRepresentation<TREE_TYPE>::LinkedObject {
 public:
  typedef boost::shared_ptr< BeliefInterface<TREE_TYPE> > Ptr;
  typedef TREE_TYPE TreeType;

  struct Config {
    std::string world_frame_name;
  };

  BeliefInterface(Config config)
  : world_frame_name_(config.world_frame_name) {}

  virtual ~BeliefInterface() {}

  double GetCellCountInBBox(Eigen::Vector3d lb, Eigen::Vector3d ub) {
    unsigned int count = 0;
    for( typename TREE_TYPE::iterator it = this->link_.octree->begin(), end = this->link_.octree->end(); it!=end; ++it ){
      double size = it.getSize();
      double x = it.getX();
      double y = it.getY();
      double z = it.getZ();

      if (x < lb.x() || x > ub.x() || y < lb.y() || x > ub.y() || z < lb.z() || x > ub.z())
        continue;

      if( this->link_.octree->isNodeOccupied(*it) )
        count++;
    }
    return count;
  }

  visualization_msgs::MarkerArray GetMarkerArray() {
    visualization_msgs::MarkerArray occupiedNodesVis;
    // each array stores all cubes of a different size, one for each depth level:
    occupiedNodesVis.markers.resize(this->link_.octree->getTreeDepth()+1);

    std_msgs::ColorRGBA color;
    color.r = 0;
    color.g = 0;
    color.b = 1;
    color.a = 1;

    for( typename TREE_TYPE::iterator it = this->link_.octree->begin(), end = this->link_.octree->end(); it!=end; ++it )
    {
      double size = it.getSize();
      double x = it.getX();
      double y = it.getY();
      double z = it.getZ();

      geometry_msgs::Point cubeCenter;
      cubeCenter.x = x;
      cubeCenter.y = y;
      cubeCenter.z = z;
      unsigned idx = it.getDepth();

      if( this->link_.octree->isNodeOccupied(*it) )
      {
        double size = it.getSize();
        assert(idx < occupiedNodesVis.markers.size());

        occupiedNodesVis.markers[idx].points.push_back(cubeCenter);
      }
    }

    for (unsigned i= 0; i < occupiedNodesVis.markers.size(); ++i)
    {
      double size = this->link_.octree->getNodeSize(i);

      occupiedNodesVis.markers[i].header.frame_id = world_frame_name_;
      occupiedNodesVis.markers[i].header.stamp = ros::Time::now();
      occupiedNodesVis.markers[i].ns = "map";
      occupiedNodesVis.markers[i].id = i;
      occupiedNodesVis.markers[i].type = visualization_msgs::Marker::CUBE_LIST;
      occupiedNodesVis.markers[i].scale.x = size;
      occupiedNodesVis.markers[i].scale.y = size;
      occupiedNodesVis.markers[i].scale.z = size;
      occupiedNodesVis.markers[i].color = color;

      if (occupiedNodesVis.markers[i].points.size() > 0)
        occupiedNodesVis.markers[i].action = visualization_msgs::Marker::ADD;
      else
        occupiedNodesVis.markers[i].action = visualization_msgs::Marker::DELETE;
    }

    return occupiedNodesVis;
  }
 private:
  std::string world_frame_name_;
};

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_H_ */
