/* Copyright 2015 Sanjiban Choudhury
 * rdf_regression_cs_classification.h
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_CS_CLASSIFICATION_RDF_REGRESSION_CS_CLASSIFICATION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_CS_CLASSIFICATION_RDF_REGRESSION_CS_CLASSIFICATION_H_

#include "ig_learning/cs_classification.h"
#include "alglib/dataanalysis.h"

namespace ig_learning {

class RDFRegression : public CSClassification {
 public:
  RDFRegression();

  virtual ~RDFRegression() {}

  virtual bool Initialize(unsigned int number_trees);

  virtual bool Train(const std::vector<Eigen::MatrixXd> &feature_table, const std::vector<Eigen::VectorXd> &qval_table);

  virtual unsigned int Predict(const Eigen::MatrixXd &feature);

  virtual bool Save(const std::string &filename);

  virtual bool Load(const std::string &filename);

  virtual CSClassification *Clone() const { return new RDFRegression(*this); } // Factory Method

 protected:
  alglib::decisionforest rdf_;
  unsigned int number_trees_;
};

typedef boost::shared_ptr<RDFRegression> RDFRegressionPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_CS_CLASSIFICATION_RDF_REGRESSION_CS_CLASSIFICATION_H_ */
