/* Copyright 2015 Sanjiban Choudhury
 * motion_cost_function.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/cost_functions/motion_cost_function.h"

namespace ig_learning {

double MotionCostFunction::Cost(const State &state, const WorldMap &world_map) const {
  double distance = 0;
  for (unsigned int i = 0; i < state.action_set.size() - 1; i++) {
    distance += (state.action_set[i+1].view.pose().position -
                 state.action_set[i].view.pose().position).norm();
  }
  return distance;
}

}


