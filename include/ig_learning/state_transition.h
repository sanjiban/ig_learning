/* Copyright 2015 Sanjiban Choudhury
 * state_transition.h
 *
 *  Created on: Jun 28, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_H_

#include "ig_learning/state.h"

namespace ig_learning {

class StateTransition {
 public:
  StateTransition() {}

  virtual ~StateTransition() {}

  virtual bool UpdateState(const State &input, const Action &action, const WorldMap &world_map, State &output) const = 0;
};

typedef boost::shared_ptr<StateTransition> StateTransitionPtr;

}


#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_H_ */
