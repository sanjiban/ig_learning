/* Copyright 2015 Sanjiban Choudhury
 * generalized_cost_benefit.h
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVERS_GENERALIZED_COST_BENEFIT_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVERS_GENERALIZED_COST_BENEFIT_H_


#include "ig_learning/offline_solver.h"

namespace ig_learning {

class GeneralizedCostBenefit : public OfflineSolver {
 public:
  GeneralizedCostBenefit() {}
  virtual ~GeneralizedCostBenefit() {}

  virtual void Solve(const OfflineSolverParams &input, OfflineSolverOutput &output) const;

 protected:
  double MotionAndTourCost(const State &state, const OfflineSolverParams &input) const;

  void RearrangeActionsOptimalTour(const State &state, const OfflineSolverParams &input, State &output) const;

  void RecomputeObjectiveCost(const State &state, const OfflineSolverParams &input, OfflineSolverOutput &output) const;
};

}


#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVERS_GENERALIZED_COST_BENEFIT_H_ */
