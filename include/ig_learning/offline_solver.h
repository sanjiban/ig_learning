/* Copyright 2015 Sanjiban Choudhury
 * offline_solver.h
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVER_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVER_H_

#include "ig_learning/state.h"
#include "ig_learning/state_transition.h"
#include "ig_learning/objective_function.h"
#include "ig_learning/cost_function.h"

namespace ig_learning {

struct OfflineSolverParams {
  ObjectiveFunctionPtr obj_fn;
  CostFunctionPtr cost_fn;
  StateTransitionPtr state_transition;
  WorldMap world_map;
  State state0;
  unsigned int budget;
  double cost_budget;
};

struct OfflineSolverOutput {
  std::vector<Action> action_sequence;
  State state_final;
  std::vector<double> objective_trajectory;
  std::vector<double> cost_trajectory;
};

class OfflineSolver {
 public:
  OfflineSolver() {}
  virtual ~OfflineSolver() {}

  virtual void Solve(const OfflineSolverParams &input, OfflineSolverOutput &output) const = 0;
};

typedef boost::shared_ptr<OfflineSolver> OfflineSolverPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVER_H_ */
