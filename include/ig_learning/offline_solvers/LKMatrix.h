#ifndef LK_MATRIX
#define LK_MATRIX

#include <vector>
//TODO: Request licencse form https://github.com/lingz/LK-Heuristic
namespace ig_learning {
namespace tsp_solver {

class LKMatrix {
  public:
    int size;
    LKMatrix(const std::vector< std::vector <double> > &edge_distances);
    std::vector<int> getCurrentTour();
    double getCurrentTourDistance();
    void optimizeTour();
    void printTour();
    void printTourIds();

  private:
    std::vector<int> tour;
    std::vector<std::vector<int> > edgeFlags;
    std::vector<int> ids;
    void joinLocations(int i, int j);
    std::vector<std::vector<double> > edgeDistances;
    void LKMove(int tourStart);
    void reverse(int start, int end);
    bool isTour();
};

}
}
#endif
