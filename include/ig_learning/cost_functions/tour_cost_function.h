/* Copyright 2015 Sanjiban Choudhury
 * tour_cost_function.h
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTIONS_TOUR_COST_FUNCTION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTIONS_TOUR_COST_FUNCTION_H_

#include "ig_learning/cost_function.h"

namespace ig_learning {

class TourCostFunction : public CostFunction {
 public:
  TourCostFunction();

  virtual ~TourCostFunction() {}

  virtual double Cost(const State &state, const WorldMap &world_map) const;
};


}




#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTIONS_TOUR_COST_FUNCTION_H_ */
