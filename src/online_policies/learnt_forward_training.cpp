/* Copyright 2015 Sanjiban Choudhury
 * learnt_forward_training.cpp
 *
 *  Created on: Jul 13, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/online_policies/learnt_forward_training.h"

namespace ig_learning {

void LearntForwardTraining::Compute(double time_step,
                                    const State &state,
                                    const std::set<Action> action_set,
                                    Action &selected_action) const {
  CSClassificationPtr model = model_set_[time_step];

  std::vector <double> tmp;
  feature_fn_->GetFeature(state, *action_set.begin(), tmp);
  unsigned int fdim = tmp.size();

  Eigen::MatrixXd feature_point(action_set.size(), fdim);
  unsigned int id = 0;
  for (auto a : action_set) {
    std::vector<double> feature_vec;
    feature_fn_->GetFeature(state, a, feature_vec);
    for (unsigned int fidx = 0; fidx < feature_vec.size(); fidx++)
      feature_point(id, fidx) = feature_vec[fidx];
    id++;
  }

  unsigned int selected_index = model->Predict(feature_point);
  selected_action = *std::next(action_set.begin(), selected_index);
}

}


