/* Copyright 2015 Sanjiban Choudhury
 * tsp_solver.h
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVERS_TSP_SOLVER_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVERS_TSP_SOLVER_H_

#include "ig_learning/state.h"

namespace ig_learning {
namespace tsp_solver {

double GetTourCost (const State &input_state);

void GetOptimizedTour (const State &input_state, State &output_state);

std::vector < std::vector <double> > GetEdgeDistance(const State &state);

}
}




#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVERS_TSP_SOLVER_H_ */
