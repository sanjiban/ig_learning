/* Copyright 2015 Sanjiban Choudhury
 * ig_heuristic_policy.h
 *
 *  Created on: Jul 13, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_IG_HEURISTIC_POLICY_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_IG_HEURISTIC_POLICY_H_

#include "ig_learning/online_policy.h"
#include "ig_active_reconstruction_octomap/octomap_ig_tree_world_representation.hpp"
#include "ig_active_reconstruction_octomap/octomap_basic_ray_ig_calculator.hpp"

namespace ig_learning {

class IGHeuristicPolicy : public OnlinePolicy {
 public:
  typedef ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation WorldRepresentation;
  typedef WorldRepresentation::TreeType TreeType;

  IGHeuristicPolicy()
  : ig_calc_config_(),
    ig_config_() {}

  virtual ~IGHeuristicPolicy() {}

  virtual void Compute(double time_step,
                       const State &state,
                       const std::set<Action> action_set,
                       Action &selected_action) const;

  virtual bool Initialize(ros::NodeHandle &nh);

 protected:
  ig_active_reconstruction::world_representation::octomap::BasicRayIgCalculator<ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation::TreeType>::Config ig_calc_config_;
  ig_active_reconstruction::world_representation::octomap::InformationGain<ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation::TreeType>::Config ig_config_;
  std::vector<double> weights_;
};

typedef boost::shared_ptr<IGHeuristicPolicy> IGHeuristicPolicyPtr;

}





#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_IG_HEURISTIC_POLICY_H_ */
