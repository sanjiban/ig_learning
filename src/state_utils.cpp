/* Copyright 2015 Sanjiban Choudhury
 * state_utils.cpp
 *
 *  Created on: Jul 7, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/state_utils.h"
#include "tf_conversions/tf_eigen.h"
#include "ig_active_reconstruction/view_space.hpp"
#include "ig_active_reconstruction_ros/param_loader.hpp"


namespace iar = ig_active_reconstruction;
using namespace iar::world_representation::octomap;

namespace ig_learning {
namespace state_utils {

void DisplayStateActionObservationSequence(const State &state, ros::Publisher &pub_pcl, tf::TransformBroadcaster &br, double dt) {
  for (std::size_t i = 0; i < state.action_set.size(); i++) {
    // Publish action
    movements::Pose pose = state.action_set[i].view.pose();
    tf::Vector3 trans;
    tf::Quaternion quat;
    tf::vectorEigenToTF(pose.position, trans);
    tf::quaternionEigenToTF(pose.orientation, quat);
    tf::Transform transform(quat, trans);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "sensor"));
    ros::Duration(dt).sleep();

    pcl::PointCloud<pcl::PointXYZ> pcl = state.observation_set[i];
    pcl.header.frame_id = "world";
    pcl.header.stamp = ros::Time::now().toSec();
    pub_pcl.publish(pcl);
    ros::Duration(dt).sleep();
  }
}

Action GetRandomAction(const std::set<Action> &action_set){
  return *std::next(action_set.begin(), std::rand() % action_set.size());
}

bool LoadWorldMap(std::string view_space_filename, std::string pcd_foldername, std::string model_filename, double res, WorldMap &world_map) {
  iar::views::ViewSpace view_space;
  view_space.loadFromFile(view_space_filename);
  if (view_space.size() == 0)
    return false;
  unsigned int id = 0;
  world_map.action_set.clear();
  for (iar::views::ViewSpace::Iterator it = view_space.begin(); it != view_space.end(); ++it, id++) {
    Action action;
    action.view = *it;
    action.id = id;
    world_map.action_set.insert(action);
  }
  world_map.pcl_lookup.clear();
  for (unsigned i = 0; i < world_map.action_set.size(); i++) {
    std::stringstream ss;
    ss << pcd_foldername << i << ".pcd";
    pcl::PointCloud<pcl::PointXYZ> cloud;
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (ss.str(), cloud) == -1) {
      return false;
    }
    world_map.pcl_lookup[i] = cloud;
  }
  if (pcl::io::loadPCDFile<pcl::PointXYZ> (model_filename, world_map.model_pcl) == -1) {
    ROS_ERROR_STREAM("Couldnt load model pcd");
    return false;
  }
  world_map.res = res;
  return true;
}

bool LoadWorldMap(ros::NodeHandle &n, WorldMap &world_map) {
  std::string viewspace_file_path;
  ros_tools::getExpParam(viewspace_file_path,"viewspace_file_path", n);

  std::string pcd_folder_path;
  ros_tools::getExpParam(pcd_folder_path,"pcd_folder", n);

  std::string model_filename;
  ros_tools::getExpParam(model_filename,"model_filename", n);

  double model_res;
  ros_tools::getExpParam(model_res,"model_res", n);

  LoadWorldMap(viewspace_file_path, pcd_folder_path, model_filename, model_res, world_map);
  return true;
}


}
}


