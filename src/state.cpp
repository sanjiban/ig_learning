/* Copyright 2015 Sanjiban Choudhury
 * state.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/state.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"

namespace ig_learning {

bool InitializeState(ros::NodeHandle &n, State &state) {
  typedef Belief::TreeType TreeType;
  TreeType::Config octree_config;
  ros_tools::getExpParam(octree_config.resolution_m,"resolution_m");
  ros_tools::getExpParam(octree_config.occupancy_threshold,"occupancy_threshold");
  ros_tools::getExpParam(octree_config.hit_probability,"hit_probability");
  ros_tools::getExpParam(octree_config.miss_probability,"miss_probability");
  ros_tools::getExpParam(octree_config.clamping_threshold_min,"clamping_threshold_min");
  ros_tools::getExpParam(octree_config.clamping_threshold_max,"clamping_threshold_max");
  state.belief = Belief(octree_config);
  return true;
}

visualization_msgs::MarkerArray VisualizeBelief(const State &input) {
  State state(input);
  typedef Belief::TreeType TreeType;
  BeliefInterface<TreeType>::Config config;
  config.world_frame_name = "world";
  BeliefInterface<TreeType>::Ptr obj = state.belief.getLinkedObj<BeliefInterface>(config);
  return obj->GetMarkerArray();
}

pcl::PointCloud<pcl::PointXYZ> VisualizeObservation(const State &state) {
  pcl::PointCloud<pcl::PointXYZ> cum_cloud;
  for (auto it: state.observation_set)
    cum_cloud += it;
  cum_cloud.header.frame_id = "world";
  cum_cloud.header.stamp = ros::Time::now().toSec();
  return cum_cloud;
}

}


