/* Copyright 2015 Sanjiban Choudhury
 * occupied_cells_objective_function.h
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_OBJECTIVE_FUNCTIONS_OCCUPIED_CELLS_OBJECTIVE_FUNCTION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_OBJECTIVE_FUNCTIONS_OCCUPIED_CELLS_OBJECTIVE_FUNCTION_H_

#include "ig_learning/objective_function.h"

namespace ig_learning {

class OccupiedCellsObjectiveFunction : public ObjectiveFunction {
 public:
  OccupiedCellsObjectiveFunction();
  OccupiedCellsObjectiveFunction(Eigen::Vector3d bbox_lb, Eigen::Vector3d bbox_ub);

  virtual ~OccupiedCellsObjectiveFunction() {}

  virtual double Value(const State &state, const WorldMap &world_map) const;
 protected:
  Eigen::Vector3d bbox_lb_, bbox_ub_;
};


}




#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_OBJECTIVE_FUNCTIONS_OCCUPIED_CELLS_OBJECTIVE_FUNCTION_H_ */
