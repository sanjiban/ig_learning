/* Copyright 2015 Sanjiban Choudhury
 * ig_feature_extractor.cpp
 *
 *  Created on: Jul 7, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/feature_extractor/ig_feature_extractor.h"
#include "ig_active_reconstruction_octomap/octomap_basic_ray_ig_calculator.hpp"
#include "ig_active_reconstruction_octomap/ig/occlusion_aware.hpp"
#include "ig_active_reconstruction_octomap/ig/unobserved_voxel.hpp"
#include "ig_active_reconstruction_octomap/ig/rear_side_voxel.hpp"
#include "ig_active_reconstruction_octomap/ig/rear_side_entropy.hpp"
#include "ig_active_reconstruction_octomap/ig/proximity_count.hpp"
#include "ig_active_reconstruction_octomap/ig/vasquez_gomez_area_factor.hpp"
#include "ig_active_reconstruction_octomap/ig/average_entropy.hpp"
#include "ig_active_reconstruction_octomap/octomap_ros_pcl_input.hpp"
#include "ig_active_reconstruction_octomap/octomap_ros_interface.hpp"

#include "ig_active_reconstruction_ros/param_loader.hpp"

namespace iar = ig_active_reconstruction;
using namespace iar::world_representation::octomap;

namespace ig_learning {

bool IGFeatureExtractor::Initialize(ros::NodeHandle &nh) {
  ros_tools::getExpParam<unsigned int,int>(ig_calc_config_.ray_caster_config.img_width_px,"img_width_px");
  ros_tools::getExpParam<unsigned int,int>(ig_calc_config_.ray_caster_config.img_height_px,"img_height_px");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.camera_matrix(0,0),"camera/fx");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.camera_matrix(1,1),"camera/fy");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.camera_matrix(0,2),"camera/cx");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.camera_matrix(1,2),"camera/cy");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.max_ray_depth_m,"max_ray_depth_m");

  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.resolution.ray_resolution_x,"raycasting/resolution_x");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.resolution.ray_resolution_y,"raycasting/resolution_y");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.resolution.min_x_perc,"raycasting/min_x_perc");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.resolution.min_y_perc,"raycasting/min_y_perc");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.resolution.max_x_perc,"raycasting/max_x_perc");
  ros_tools::getExpParam(ig_calc_config_.ray_caster_config.resolution.max_y_perc,"raycasting/max_y_perc");

  ros_tools::getExpParam(ig_config_.p_unknown_prior,"ig/p_unknown_prior");
  ros_tools::getExpParam(ig_config_.p_unknown_upper_bound,"ig/p_unknown_upper_bound");
  ros_tools::getExpParam(ig_config_.p_unknown_lower_bound,"ig/p_unknown_lower_bound");
  ros_tools::getExpParam<unsigned int,int>(ig_config_.voxels_in_void_ray,"ig/voxels_in_void_ray");

  return true;
}

void IGFeatureExtractor::GetFeature(const State &input_state, const Action &action, std::vector<double> &feature) const {
  feature.clear();
  State state(input_state);
  BasicRayIgCalculator<IgTreeWorldRepresentation::TreeType>::Ptr ig_calculator = state.belief.getLinkedObj<BasicRayIgCalculator>(ig_calc_config_);

  ig_calculator->registerInformationGain<OcclusionAwareIg>(ig_config_);
  ig_calculator->registerInformationGain<UnobservedVoxelIg>(ig_config_);
  ig_calculator->registerInformationGain<RearSideVoxelIg>(ig_config_);
  ig_calculator->registerInformationGain<RearSideEntropyIg>(ig_config_);
  ig_calculator->registerInformationGain<ProximityCountIg>(ig_config_);
  ig_calculator->registerInformationGain<VasquezGomezAreaFactorIg>(ig_config_);
  ig_calculator->registerInformationGain<AverageEntropyIg>(ig_config_);

  iar::world_representation::CommunicationInterface::ViewIgResult information_gains;
  iar::world_representation::CommunicationInterface::IgRetrievalCommand command;
  double ig_val = 0;

  command.path.clear();
  command.path.push_back( action.view.pose() );
  command.metric_names.push_back("OcclusionAwareIg");
  command.metric_names.push_back("UnobservedVoxelIg");
  command.metric_names.push_back("RearSideVoxelIg");
  command.metric_names.push_back("RearSideEntropyIg");
  command.metric_names.push_back("ProximityCountIg");
  command.metric_names.push_back("VasquezGomezAreaFactorIg");
  command.metric_names.push_back("AverageEntropyIg");

  ig_calculator->computeViewIg(command, information_gains);

  for (std::size_t i = 0; i < information_gains.size(); i++)
    feature.push_back(information_gains[i].predicted_gain);
}


}


