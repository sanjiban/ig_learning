/* Copyright 2015 Sanjiban Choudhury
 * last_vertex_pose_feature_extractor.h
 *
 *  Created on: Jul 7, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_LAST_VERTEX_POSE_FEATURE_EXTRACTOR_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_LAST_VERTEX_POSE_FEATURE_EXTRACTOR_H_

#include "ig_learning/feature_extractor.h"

namespace ig_learning {

class LastVertexPoseFeatureExtractor : public FeatureExtractor {
 public:
  enum mode {ABS_POS_ONLY = 0, REL_POS_ONLY, ABS_POS_ORIENT, REL_POS_ORIENT, REL_POS_ORIENT_MAG};

  LastVertexPoseFeatureExtractor(mode m)
  : mode_(m){}
  virtual ~LastVertexPoseFeatureExtractor() {}

  virtual void GetFeature(const State &state, const Action &action, std::vector<double> &feature) const;
 protected:
  mode mode_;
  Eigen::Vector3d RPYFromQuat(const Eigen::Quaterniond &quat) const;
};
typedef boost::shared_ptr<LastVertexPoseFeatureExtractor> LastVertexPoseFeatureExtractorPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_LAST_VERTEX_POSE_FEATURE_EXTRACTOR_H_ */
