/* Copyright 2015 Sanjiban Choudhury
 * ig_feature_extractor.h
 *
 *  Created on: Jul 7, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_IG_FEATURE_EXTRACTOR_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_IG_FEATURE_EXTRACTOR_H_

#include "ig_learning/feature_extractor.h"
#include "ig_active_reconstruction_octomap/octomap_ig_tree_world_representation.hpp"
#include "ig_active_reconstruction_octomap/octomap_basic_ray_ig_calculator.hpp"

namespace ig_learning {

class IGFeatureExtractor : public FeatureExtractor {
 public:
  typedef ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation WorldRepresentation;
  typedef WorldRepresentation::TreeType TreeType;

  IGFeatureExtractor()
  : ig_calc_config_(),
    ig_config_() {}
  virtual ~IGFeatureExtractor() {}

  virtual void GetFeature(const State &state, const Action &action, std::vector<double> &feature) const;

  bool Initialize(ros::NodeHandle &nh);

 protected:
  ig_active_reconstruction::world_representation::octomap::BasicRayIgCalculator<ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation::TreeType>::Config ig_calc_config_;
  ig_active_reconstruction::world_representation::octomap::InformationGain<ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation::TreeType>::Config ig_config_;
};
typedef boost::shared_ptr<IGFeatureExtractor> IGFeatureExtractorPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_IG_FEATURE_EXTRACTOR_H_ */
