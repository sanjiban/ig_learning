/* Copyright 2015 Sanjiban Choudhury
 * tour_cost_function.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/cost_functions/tour_cost_function.h"
#include "ig_learning/offline_solvers/tsp_solver.h"

namespace ig_learning {

double TourCostFunction::Cost(const State &state, const WorldMap &world_map) const {
  return tsp_solver::GetTourCost(state);
}

}


