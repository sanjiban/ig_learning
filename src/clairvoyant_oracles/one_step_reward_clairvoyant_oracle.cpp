/* Copyright 2015 Sanjiban Choudhury
 * one_step_reward_clairvoyant_oracle.cpp
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/clairvoyant_oracles/one_step_reward_clairvoyant_oracle.h"

namespace ig_learning {

double OneStepRewardClairvoyantOracle::Value(const State &state, const Action &action, const WorldMap &world_map, unsigned int time_steps) const {
  State new_state;
  state_obs_transition_->UpdateState(state, action, world_map, new_state);
  return obj_fn_->Value(new_state, world_map) - obj_fn_->Value(state, world_map);
}


}


