/* Copyright 2015 Sanjiban Choudhury
 * pcl_voxel_match_objective_function.cpp
 *
 *  Created on: Jul 5, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/objective_functions/pcl_voxel_match_objective_function.h"
#include <pcl/octree/octree.h>
#include <pcl/octree/octree_pointcloud_occupancy.h>
#include <pcl/octree/octree_impl.h>

namespace ig_learning {

double PclVoxelMatchObjectiveFunction::Value(const State &state, const WorldMap &world_map) const {
  pcl::octree::OctreePointCloudOccupancy<pcl::PointXYZ> octree (world_map.res);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cum_cloud(new pcl::PointCloud<pcl::PointXYZ>());
  for (auto it : state.observation_set)
    *cum_cloud += it;
  octree.setInputCloud(cum_cloud);
  octree.addPointsFromInputCloud();

  unsigned int count = 0;
  for (auto it : world_map.model_pcl.points)
    if(octree.isVoxelOccupiedAtPoint(it))
      count ++;
  return (double) count / (double) world_map.model_pcl.points.size();
}


}


