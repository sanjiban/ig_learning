/* Copyright 2015 Sanjiban Choudhury
 * objective_function.h
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_OBJECTIVE_FUNCTION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_OBJECTIVE_FUNCTION_H_

#include "ig_learning/state.h"

namespace ig_learning {

class ObjectiveFunction {
 public:
  ObjectiveFunction() {}
  virtual ~ObjectiveFunction() {}

  virtual double Value(const State &state, const WorldMap &world_map) const = 0;
};

typedef boost::shared_ptr<ObjectiveFunction> ObjectiveFunctionPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_OBJECTIVE_FUNCTION_H_ */
