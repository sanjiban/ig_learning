/* Copyright 2015 Sanjiban Choudhury
 * pcl_voxel_match_objective_function.h
 *
 *  Created on: Jul 5, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_DOMAIN_SPECIFIC_FUNCTIONS_PCL_VOXEL_MATCH_OBJECTIVE_FUNCTION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_DOMAIN_SPECIFIC_FUNCTIONS_PCL_VOXEL_MATCH_OBJECTIVE_FUNCTION_H_


#include "ig_learning/objective_function.h"

namespace ig_learning {

class PclVoxelMatchObjectiveFunction : public ObjectiveFunction {
 public:
  PclVoxelMatchObjectiveFunction() {}

  virtual ~PclVoxelMatchObjectiveFunction() {}

  virtual double Value(const State &state, const WorldMap &world_map) const;
 protected:
};

typedef boost::shared_ptr<PclVoxelMatchObjectiveFunction> PclVoxelMatchObjectiveFunctionPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_DOMAIN_SPECIFIC_FUNCTIONS_PCL_VOXEL_MATCH_OBJECTIVE_FUNCTION_H_ */
