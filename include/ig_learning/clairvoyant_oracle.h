/* Copyright 2015 Sanjiban Choudhury
 * clairvoyant_oracle.h
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_CLAIRVOYANT_ORACLE_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_CLAIRVOYANT_ORACLE_H_

#include "ig_learning/state.h"

namespace ig_learning {

class ClairvoyantOracle {
 public:
  ClairvoyantOracle() {}
  virtual ~ClairvoyantOracle() {}

  virtual double Value(const State &state, const Action &action, const WorldMap &world_map, unsigned int time_steps) const = 0;

};

typedef boost::shared_ptr<ClairvoyantOracle> ClairvoyantOraclePtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_CLAIRVOYANT_ORACLE_H_ */
