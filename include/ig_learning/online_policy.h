/* Copyright 2015 Sanjiban Choudhury
 * online_policy.h
 *
 *  Created on: Jul 13, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICY_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICY_H_

#include "ig_learning/state.h"

namespace ig_learning {

class OnlinePolicy {
 public:
  OnlinePolicy() {}
  virtual ~OnlinePolicy() {}

  virtual void Compute(double time_step,
                       const State &state,
                       const std::set<Action> action_set,
                       Action &action) const = 0;
};

typedef boost::shared_ptr<OnlinePolicy> OnlinePolicyPtr;

}





#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICY_H_ */
