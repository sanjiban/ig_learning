/* Copyright 2015 Sanjiban Choudhury
 * one_step_reward_clairvoyant_oracle.h
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_CLAIRVOYANT_ORACLES_ONE_STEP_REWARD_CLAIRVOYANT_ORACLE_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_CLAIRVOYANT_ORACLES_ONE_STEP_REWARD_CLAIRVOYANT_ORACLE_H_

#include "ig_learning/clairvoyant_oracle.h"
#include "ig_learning/objective_function.h"
#include "ig_learning/state_transition.h"

namespace ig_learning {

class OneStepRewardClairvoyantOracle : public ClairvoyantOracle {
 public:
  OneStepRewardClairvoyantOracle(ObjectiveFunctionPtr obj_fn, StateTransitionPtr state_obs_transition)
 : obj_fn_(obj_fn),
   state_obs_transition_(state_obs_transition) {}

  virtual ~OneStepRewardClairvoyantOracle() {}

  virtual double Value(const State &state, const Action &action, const WorldMap &world_map, unsigned int time_steps) const;
protected:
  ObjectiveFunctionPtr obj_fn_;
  StateTransitionPtr state_obs_transition_;
};

typedef boost::shared_ptr<OneStepRewardClairvoyantOracle> OneStepRewardClairvoyantOraclePtr;


}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_CLAIRVOYANT_ORACLES_ONE_STEP_REWARD_CLAIRVOYANT_ORACLE_H_ */
