/* Copyright 2015 Sanjiban Choudhury
 * aggrevate.h
 *
 *  Created on: Jul 29, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_IMITATION_LEARNING_AGGREVATE_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_IMITATION_LEARNING_AGGREVATE_H_

#include "ig_learning/state.h"
#include "ig_learning/cs_classification.h"
#include "ig_learning/state_transition.h"
#include "ig_learning/objective_function.h"
#include "ig_learning/cost_function.h"
#include "ig_learning/clairvoyant_oracle.h"
#include "ig_learning/feature_extractor.h"

namespace ig_learning {

class Aggrevate {
 public:
  struct Input {
    struct DataPoint {
      State state0;
      WorldMap map;
      std::vector<Action> precomputed_expert;
    };
    std::vector< DataPoint > train_dataset;
    std::vector< std::pair<State, WorldMap> > validation_dataset;
    unsigned int total_timesteps;
  };

  struct Output {
    std::vector <CSClassificationPtr> model_set;
    std::vector<double> train_error;
    std::vector<double> validation_error;
    std::vector< std::vector<double> > objective_trajectory_set;
  };

  class ActionSelection {
   public:
    enum mode { SUBSET = 0, ALL};

    ActionSelection()
    : mode_(mode::SUBSET),
      num_selection_(1) {}

    ActionSelection(mode m)
    : mode_(m),
      num_selection_(1) {}

    ActionSelection(mode m, unsigned int num_selection)
    : mode_(m),
      num_selection_(num_selection) {}

    ~ActionSelection(){}

    std::set<Action> SelectActions(const std::set<Action> &action_selection) const;
   private:
    unsigned int num_selection_;
    mode mode_;
  };

  struct Parameters {
    StateTransitionPtr state_belief_transition;
    ObjectiveFunctionPtr objective_fn;
    CostFunctionPtr cost_fn;
    double cost_budget;
    ClairvoyantOraclePtr oracle_fn;
    FeatureExtractorPtr feature_fn;
    CSClassificationPtr cs_class_fn;
    ActionSelection action_selection;

    unsigned int max_iters_aggrevate;
    std::function<double (unsigned int)> beta_fn = [](unsigned int i) {return i == 0 ? 1.0 : 0.0; };
    std::string model_filename_prefix;
  };

  Aggrevate(Parameters params)
  :params_(params) {}

  ~Aggrevate() {}

  void Train(const Input &input, Output &output) const;
 private:

  void RollOutLearner(const State &state0, const WorldMap &world_map, double tau, const CSClassificationPtr &model, State &state) const;

  void RollOutPrecomputedOracle(const State &state0, const WorldMap &world_map, double tau, const std::vector<Action> &action_sequence, State &state) const;

  Parameters params_;
};

}





#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_IMITATION_LEARNING_AGGREVATE_H_ */
